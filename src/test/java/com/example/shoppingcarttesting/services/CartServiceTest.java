package com.example.shoppingcarttesting.services;

import com.example.shoppingcarttesting.models.Cart;
import com.example.shoppingcarttesting.models.Item;
import com.example.shoppingcarttesting.repository.CartRepository;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Arrays;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;


public class CartServiceTest {
    @Mock
    CartRepository cartRepository;
    @InjectMocks
    CartService cartService;
    @Test
    public void returnCart(){
        when(cartRepository.findAll()).thenReturn(Arrays.asList(new Cart(1,1,12)));
        assertEquals("milk",cartService.listCartItems().getcartItems().get(1).getProduct());
    }

    @Test
    public void deleteItem(){
        Item st = new Item(5,"food",120);
        when(cartRepository.findById(Math.toIntExact(st.getId()))).thenReturn(Optional.of(st));
        cartService.deleteCartItem(Math.toIntExact(st.getId()));
        verify(cartRepository).deleteById(Math.toIntExact(st.getId()));

    }

    @Test
    public void addToCart(){
        when(cartRepository.save(ArgumentMatchers.any(Cart.class))).thenReturn(new Cart(1,"milk",45));
        assertEquals("meat",cartService.addToCart("milk","milk",4).getGender());
    }
}
