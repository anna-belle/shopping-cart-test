package com.example.shoppingcarttesting;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ShoppingCartTestingApplication {

    public static void main(String[] args) {
        SpringApplication.run(ShoppingCartTestingApplication.class, args);
    }

}
