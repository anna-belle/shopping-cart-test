package com.example.shoppingcarttesting.services;

import com.example.shoppingcarttesting.dto.cart.AddToCartDto;
import com.example.shoppingcarttesting.dto.cart.CartDto;
import com.example.shoppingcarttesting.dto.cart.CartItemDto;
import com.example.shoppingcarttesting.models.Cart;
import com.example.shoppingcarttesting.models.Item;
import com.example.shoppingcarttesting.repository.CartRepository;
import com.example.shoppingcarttesting.repository.ItemRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.*;

@Service
public class CartService {

    @Autowired
    CartRepository cartRepository;
    public CartService(){}

    public CartService(CartRepository cartRepository) {
        this.cartRepository = cartRepository;
    }



    public void addToCart(AddToCartDto addToCartDto, Item product){
        Cart cart = new Cart(product, addToCartDto.getQuantity());
        cartRepository.save(cart);
    }


    public CartDto listCartItems() {
        List<Cart> cartList = cartRepository.findAll();
        List<CartItemDto> cartItems = new ArrayList<>();
        for (Cart cart:cartList){
            CartItemDto cartItemDto = getDtoFromCart(cart);
            cartItems.add(cartItemDto);
        }
        double totalCost = 0;
        for (CartItemDto cartItemDto :cartItems){
            totalCost += (cartItemDto.getProduct().getPrice() * cartItemDto.getQuantity());
        }
        return new CartDto(cartItems,totalCost);
    }


    public static CartItemDto getDtoFromCart(Cart cart) {
        return new CartItemDto(cart);
    }


    public void updateCartItem(AddToCartDto cartDto,Item product){
        Cart cart = cartRepository.getOne(cartDto.getId());
        cart.setQuantity(cartDto.getQuantity());
        cart.setCreatedDate(new Date());
        cartRepository.save(cart);
    }

    public void deleteCartItem(int id) {
        cartRepository.deleteById(id);
    }

}
