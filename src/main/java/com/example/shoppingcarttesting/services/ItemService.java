package com.example.shoppingcarttesting.services;


import com.example.shoppingcarttesting.dto.product.ProductDto;
import com.example.shoppingcarttesting.models.Item;
import com.example.shoppingcarttesting.repository.ItemRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;




import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class ItemService {

    @Autowired
    ItemRepository itemRepository;

    public List<ProductDto> listProducts() {
        List<Item> products = itemRepository.findAll();
        List<ProductDto> productDtos = new ArrayList<>();
        for(Item product : products) {
            ProductDto productDto = getDtoFromProduct(product);
            productDtos.add(productDto);
        }
        return productDtos;
    }

    public static ProductDto getDtoFromProduct(Item product) {
        ProductDto productDto = new ProductDto(product);
        return productDto;
    }

    public static Item getProductFromDto(ProductDto productDto) {
        Item product = new Item(productDto);
        return product;
    }

    public void addProduct(ProductDto productDto) {
        Item product = getProductFromDto(productDto);
        itemRepository.save(product);
    }

    public void updateProduct(Long productID, ProductDto productDto) {
        Item product = getProductFromDto(productDto);
        product.setId(productID);
        itemRepository.save(product);
    }


    public Item getProductById(Long productId) {
        Optional<Item> optionalProduct = itemRepository.findById(Long.valueOf(productId));
        return optionalProduct.get();
    }


}
}
