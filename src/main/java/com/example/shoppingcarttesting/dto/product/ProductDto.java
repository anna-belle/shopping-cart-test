package com.example.shoppingcarttesting.dto.product;

import com.example.shoppingcarttesting.models.Item;
import com.sun.istack.NotNull;

import java.math.BigDecimal;

public class ProductDto {

    private Integer id;
    private @NotNull
    String name;
    private @NotNull
    BigDecimal price;

    public ProductDto(Item product) {
        this.setId(Math.toIntExact(product.getId()));
        this.setName(product.getName());
        this.setPrice(product.getPrice());
    }

    public ProductDto(@NotNull String name, @NotNull BigDecimal price) {
        this.name = name;
        this.price = price;

    }

    public ProductDto() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

}
