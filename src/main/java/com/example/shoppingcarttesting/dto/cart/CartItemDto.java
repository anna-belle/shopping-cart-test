package com.example.shoppingcarttesting.dto.cart;

import com.example.shoppingcarttesting.models.Cart;
import com.example.shoppingcarttesting.models.Item;
import com.sun.istack.NotNull;

public class CartItemDto {
    private Integer id;
    private @NotNull
    Integer quantity;
    private @NotNull
    Item product;

    public CartItemDto() {
    }

    public CartItemDto(Cart cart) {
        this.setId(cart.getId());
        this.setQuantity(cart.getQuantity());
        this.setProduct(cart.getItem());
    }

    @Override
    public String toString() {
        return "CartDto{" +
                "id=" + id +
                ", quantity=" + quantity +
                ", productName=" + product.getName() +
                '}';
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }
    public Item getProduct() {
        return product;
    }

    public void setProduct(Item product) {
        this.product = product;
    }

}
